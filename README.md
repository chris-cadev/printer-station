## Project big picture

### Real needs

- usb driver for printers (golang)
- api to connect the driver and web client (featherjs)
- web client with functional interface (vuejs)

### Nice to have:

- module to connect with bluetooth and allow print by the admin of the service (golang)

## Roadmap

- [ ] Develop USB driver for printers in Go
  - [ ] Use existing libraries or write your own driver
- [ ] Develop API to connect driver and web client in FeatherJS
  - [ ] Define endpoints for printer functions to expose via the API
- [ ] Build web client in VueJS
  - [ ] Use a UI framework like Vuetify or Bootstrap to speed up development
- [ ] Add Bluetooth module to Go code (optional)
  - [ ] Research Bluetooth API for your platform
  - [ ] Integrate with existing Go code
