from flask import Flask, request, jsonify
import cups

app = Flask(__name__)

# Initialize the CUPS connection
conn = cups.Connection()

# Define the Flask endpoints
@app.route('/printers', methods=['GET'])
def get_printers():
    printers = conn.getPrinters()
    return jsonify(printers)

@app.route('/jobs', methods=['GET'])
def get_jobs():
    jobs = conn.getJobs()
    return jsonify(jobs)

@app.route('/submit_job', methods=['POST'])
def submit_job():
    file = request.files['file']
    printer_name = request.form['printer_name']
    title = request.form['title']
    options = request.form.getlist('options')
    print_id = conn.printFile(printer_name, file.filename, title, options)
    return jsonify(print_id)

@app.route('/cancel_job/<int:job_id>', methods=['DELETE'])
def cancel_job(job_id):
    conn.cancelJob(job_id)
    return jsonify({'message': 'Job cancelled'})

# Start the Flask app
if __name__ == '__main__':
    app.run()
